**UP DOCKER**

```js
  cd backend && npm install
  cd frontend && npm install
```

```sh
docker-compose up -d --build
```

**UP SERVER MANUAL**
```js
  cd backend && npm run dev:server
  cd frontend && npm run dev:server
```