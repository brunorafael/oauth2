export class UserGoogleDTO {
  public id: string | undefined;
  public name: string | undefined;
  public email: string | undefined;
}